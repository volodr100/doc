<?xml version="1.0" encoding="UTF-8"?>

<!DOCTYPE article PUBLIC "-//OASIS//DTD DocBook XML V4.2//EN"
  "/usr/share/sgml/docbook/dtd/xml/4.2/docbookx.dtd" []>
<!-- $Id$ -->
<article>
  <articleinfo>
    <title>Arabic Documentation Standards</title>
    <author>
      <firstname>Mohammed</firstname>
      <surname>Elzubeir</surname>
      <affiliation>
        <orgname>Arabeyes Project</orgname>
        <address><email>contact (at) arabeyes dot org</email></address>
      </affiliation>
    </author>
    <address><email>elzubeir (at) arabeyes dot org</email></address>
    <pubdate>$Revision$</pubdate>
    <abstract><para>This document will hopefully set some standards
for those involved in Arabic technical documentation and translation.</para>
    </abstract>
  </articleinfo>


<sect1 id="intro"><title>Introduction</title>
  <sect2 id="acknowledge"><title>Acknowledgments</title>
    <para>
This paper could not have been written without Chahine Hamila
(hchahine (at) magic dot fr) and Mustafa Merza (merza (at) leb dot net). In 
fact, this draft is a compilation of exchanged postings between Chahine, Merza,
and myself, edited and formatted.
    </para>
  </sect2>
</sect1>
  
<sect1 id="problems"><title>The Problems</title>
  <para>
The Arabic language is <emphasis>diglossic</emphasis>, which is a sign of 
richness and complexity. If it has not been for the preservation of 
<emphasis>Mother Arabic</emphasis>, the Arabic language would have been as 
diverse as the European languages today<footnote><para>True bilinguals, as 
research into the cognitive aspects of language acquisition shows, tend to 
show better problem solving skills.</para></footnote>.
  </para>
  <para>
Language syntactic, phonological, and semantic change is a natural law and is
unavoidable. The metaphor <emphasis>mother tongue</emphasis> could be taken 
literally, as it ages and dies while its daughters go to become languages, and 
eventually evolve to be mothers.
  </para>
  <para>
The <emphasis>elixir formula</emphasis> is clear in the case of diglossia in 
the Arabic language, as it preserved its integrity. It also provided a medium 
ground that can accommodate change. The true elixir in the case of Arabic is 
the <emphasis>Quran</emphasis><footnote><para>Regardless of your religious 
belief, the Quran is the heart of why this formula exists in the first place. 
Not only was it at the heart of maintaining the link between the daughters 
(RA's) but it also had its impact on other languages (such as Urdu, Farsi, 
etc)</para></footnote>.
  </para>
  <para>
We will define the relationships of the Arabic language classes in the
following manner:
  </para>
  <itemizedlist>
    <listitem><para>MA - Mother Arabic</para></listitem>
    <listitem><para>SA - Standard Arabic - the medium ground where heritage 
                      and language laws negotiate.</para></listitem>
    <listitem><para>RA - Regional Arabic</para></listitem>
  </itemizedlist>
  <para>
To better demonstrate the above concepts, have a look at the list in
<link linkend="balance">The Balance</link>.
  </para>
  <para>
There are roughly two major <emphasis>intelligibility</emphasis> areas in the 
Arab World:
  </para>
  <orderedlist>
    <listitem>
      <para>Maghreb - covering the majority of the Western areas.</para>
    </listitem><listitem>
      <para>Middle East - covering the rest of the Arab world.</para>
    </listitem>
  </orderedlist>
  <para>
Although the Arabic language is preserved, it is threatened to be a purely
liturgical language, refusing the evolution to our modern needs. In this new
era of mass communications, many languages face the threat of disappearing.
  </para>
  <para>
Some of the factors that may influence the survival of a language are:
  </para>
  <orderedlist>
    <listitem>
      <para>Broad geographical native use</para>
    </listitem><listitem>
      <para>Alphabets - for example, Chinese does not have alphabets.</para>
    </listitem>
  </orderedlist>

  <sect2 id="ma-ra-difference"><title>Difference Between MA and RA</title>
    <para>
There are several differences between <emphasis>M</emphasis>other 
<emphasis>A</emphasis>rabic and <emphasis>R</emphasis>egional 
<emphasis>A</emphasis>rabic. They can be categorized in three levels, 
syntactic, phonological, and semantic.
    </para>
    <itemizedlist>
      <listitem>
        <para>
Syntactic - One of the major differences between the MA and RA's is that RA's 
drop the <emphasis>case</emphasis>. The <emphasis>case</emphasis> defines 
whether the sentence is <emphasis>nominative</emphasis> or 
<emphasis>accusative</emphasis>.
        </para>
        <para>
Had it not been for the <emphasis>elixir</emphasis> formula, RA's would have 
been even further polarized to model the Latin language and its daughters
<footnote><para>Latin and Arabic are not governed by the same standards. 
Indo-European languages depend on the vowels to carry its semantic load, unlike
Semitic languages.</para></footnote>.
        </para>
      </listitem>
      <listitem><para>Phonological</para></listitem>
      <listitem><para>Semantic</para></listitem>
    </itemizedlist>
  </sect2>

  <sect2 id="word-for-word"><title>Word-for-Word in Computer Software</title>
    <para>
Unfortunately, much of the available Arabized software seems to have a
different idea of what translation entails. Although one loses certain aspects
of the meaning and context, word-for-word translations look and sound
unnatural and drive users away.
    </para>
    <para>
This phenomenon is evident in the younger generation who refuse to use Arabized
software because of the awkwardness of the terminology.
    </para>
  </sect2>
  <sect2 id="merging"><title>Merging Spoken and Written Arabic</title>

    <sect3 id="imports"><title>The Question of Imports</title>
      <para>
Importing new words into a language is a dangerous task. It opens the door for
the corruption of the language's integrity. However, it also can enrich the
language and accelerate it's evolutionary process. Translators are to treat
this with extreme care.
      </para>
      <para>
Resistance from the Arab academia and intellectuals has accompanied the
care-free approach that is taken toward importing foreign words. This practice
has lessened the acceptability of Arabized equivalents. 
      </para>
      <para>
However, in certain instances the use of borrowed terms is most appropriate.
      </para>
    </sect3>

    <sect3 id="current-lit"><title>Current Arabic Computer Literature</title>
      <para>
Current computer literature has both odd and awkward terminology, rendering it
often unintelligible. This is a result of two factors:
      </para>
      <itemizedlist>
        <listitem><para>
Arabic Romanticism - Arabic has been considered (and some still argue) to be a 
language only suitable for fiction. One of the elements which persuade this 
line of thought is the <emphasis>School of Mahjar</emphasis>.
        </para></listitem>
        <listitem><para>
Post-colonialism - Arab countries have suffered a long and enduring colonial 
period. This has resulted in drastic shifts in RA's.
        </para></listitem>
      </itemizedlist>
      <para>
Fortunately, the lack of legacy among the currently available computer Arabic
literature is to our advantage to design a wiser organized standard.
      </para>

    </sect3>
  </sect2>
</sect1>

<sect1 id="standards"><title>Standards</title>
  <para>
In order to avoid the creation of a new hybrid Arabic, which would somehow
include an intersection of RA's with SA, we will bring the gap between the two
a little closer. 
  </para>

  <sect2 id="balance"><title>The Balance of SA and RA</title>
    <para>
In the process of Arabic technical documentation, a balance between both tiers
(SA and RA) is wiser than emphasizing one over the other. In our technical
context, we must examine the elixir formula, deducing solutions based on its
responsibility for the survival of Arabs as a culture.
    </para>
    <para>
Technical Arabization falls under the media usage. Please refer to Table 1
for a list of Arabic classifications. This should not be an obstacle to
simplification, in fact it is a vehicle toward simplification. In effect, the
function of the SA is to act as the intelligible medium to Arabic in any given
environment and at any level of education.
    </para>

    <simplelist type="horiz" columns="2">
      <member><emphasis>Type</emphasis></member>
      <member><emphasis>Example</emphasis></member>
      <member>Mother Arabic</member>
      <member>Quran</member>
      <member>Standard Arabic</member>
      <member>Media</member>
      <member>Regional Arabic</member>
      <member>Conversations</member>
    </simplelist>

    <para>
Another issue to consider is the user's familiarity with the English language
and prior knowledge of existing computer applications. The borrowing
(discussed in more detail later) is here advised, as it accelerates the user's
ability to read and understand unarabized computer literature (e.g. man pages,
etc.)
    </para>
    <para>
A good balance of SA, RA, and borrowing, as well as providing the user with
the proper tools to independently accumulate a larger vocabulary from the
target language (English, French, etc.) is the recipe for technical
Arabization. 
    </para>
  </sect2>
  <sect2 id="harakat"><title>The Use of Harakat (Ligatures)</title>
    <para>
Harakat are generally omitted from literature in Standard Arabic. However,
there are special cases where they are mandatory, especially in translations
of UI's (User Interfaces).
    </para>
    <itemizedlist>
      <listitem><para>If the 'haraka' is a shadda then it is mandatory
to place. Consider the shadda to be the equivalent of a character itself.
      </para></listitem>
      <listitem><para>If the omission or addition of a haraka can change the
meaning of the word. For example, the word قران and قرآن are fundamentally different.
      </para></listitem>
    </itemizedlist>
  </sect2>

  <sect2 id="verbforms"><title>Verb Forms</title>
    <para>
In the translation of a UI, the verb forms play a fundamental role. In a survey
of what the users would like to see, the overwhelming majority prefer the
noun form as opposed to the imperative form.
    </para>
    <para>
For this reason, the following rule is to be followed: If the user is performing
a task that will hopefully result in feedback from the application or system,
the noun form is to be used. However, if the system is the one expecting the
user to provide input, the imperative form is used.  
    </para>
    <para>
For example, in the case of a "File->Open" operation, it would be in noun form.
However, if the system is asking the user to "Enter Username", the imperative
form is to be used. 
    </para>
  </sect2>
  <sect2 id="foreignnames"><title>Foreign City and Person Names</title>
    <para>
Foreign city and person names should be translated as commonly known,
referenced against the following references:
   </para>
   <itemizedlist>
     <listitem><para><ulink url="http://qamoose.arabeyes.org/">QamMoose</ulink></para></listitem>
   </itemizedlist>
   <para> 
If the name is not found then it is to be transliterated and inputted into 
QaMoose for future references. 
   </para>

  </sect2>
  <sect2 id="borrowing"><title>Borrowing Latin Words</title>
    <para>
Arab scholars have a tendency to be strongly conservative against the
borrowing from other languages. In contrast, the English language has gained
more words than any other existing language today<footnote><para>A fact that is
conveniently omitted during our public education. English has about 700,000
words whereas Arabic only has about 200,000.</para></footnote>.
    </para>
    <para>
<emphasis>Borrowing should be limited to an absolute minimum and left as a last
resort.</emphasis>
    </para>

    <sect3 id="common-law"><title>The Common Popular Law</title>
      <para>
This law says that borrowing should only occur when the resulting translation
would go against the <emphasis>common popular</emphasis>. That is to avoid the 
awkwardness which plagues our current Arabic technical literature.
      </para>
      <para>
For example, the English words <emphasis>CD</emphasis>, 
<emphasis>Internet</emphasis>, <emphasis>disk</emphasis> are so commonly used 
that it would be a more likely candidate than an inferior translation such as القرص المدمج.
      </para>
    </sect3>
  </sect2>
  <sect2 id="interchange"><title>Interchangeable Terms</title>
    <para>
In the case where two common terms are used to describe one thing (e.g.
[حاسوب  and كمبيوتر], it is left up to the translator. The
scarcity of Arabic computer literature legacy allows for a greater degree of
freedom.
    </para>
    <para>
However, translators are advised against the abuse of this option. Although
interchangeability can help when reading different material, it can lead to a
lot of unnecessary confusion. When unsure, put the additional terms between
parenthesis to clarify the usage.
    </para>
  </sect2>
  <sect2 id="abbrevs"><title>Abbreviations and Acronyms</title>
    <para>
There are many acronyms and abbreviations in computer and technical 
terminology, which makes it difficult to introduce Arabic equivalents. To 
resolve this issue the technical writer is to do the following:
    </para>
    <itemizedlist>
      <listitem><para>
Product names should always be transliterated in Arabic characters. This is
regardless of whether they are an acronym or an abbreviation.
      </para></listitem>
      <listitem><para>
All other acronyms (e.g. HTML, SGML, HTTP) should be transcribed in their
Latin form (using English characters), but their translated antonyms could 
be added between parenthesis for clarification.
      </para></listitem>
    </itemizedlist>
  </sect2>
</sect1>

</article>
