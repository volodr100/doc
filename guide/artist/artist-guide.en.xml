<?xml version="1.0" encoding="UTF-8"?>
                                                                                
<!DOCTYPE article PUBLIC "-//OASIS//DTD DocBook XML V4.2//EN"
  "/usr/share/sgml/docbook/dtd/xml/4.2/docbookx.dtd" []>
<!-- $Id$ -->
<article>
  <articleinfo>
    <title>Artist Guide</title>
    <author>
      <firstname>Mohammed</firstname>
      <surname>Elzubeir</surname>
      <affiliation>
        <orgname>Arabeyes Project</orgname>
        <address><email>contact (at) arabeyes dot org</email></address>
      </affiliation>
    </author>
    <address><email>elzubeir (at) arabeyes dot org</email></address>
    <pubdate>$Revision$</pubdate>
    <abstract>
      <para>
This document serves as a guide to any artistic related work for the Arabeyes
Project.
      </para>
    </abstract>
  </articleinfo>

<sect1 id="license"><title>License</title>
  <para>
Copyright (c) 2003, Arabeyes Project, Mohammed Elzubeir.
  </para>
  <para>
Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2
or any later version published by the Free Software Foundation;
with no Invariant Sections, no Front-Cover Texts, and no Back-Cover
Texts.  A copy of the license is included in the section entitled "GNU
Free Documentation License".
  </para>
</sect1>

<sect1 id="intro"><title>Introduction</title>
  <para>
Every now and then we are fortunate enough to run across talented invidividuals
who can make great artistic contributions to our project. This is done by
designing logos, banners, icons, fonts, etc.
  </para>
  <para>
We will touch on those subjects and try to expand on the ones that need
elaboration. However, most of it is very straight-forward and assumes that
you are already familiar with a certain set of tools of your choosing.
We will make recommendations but will not explain how to use those tools
here, as it is outside the scope of this document.
  </para>
  <sect2 id="credits"><title>Credits</title>
    <para>
One must thank and credit those who have contributed directly and/or indirectly
in this document.
    </para>
    <itemizedlist>
      <listitem><para>
Roozbeh Pournader - for his post on the
<ulink url="http://lists.arabeyes.org/mailman/listinfo/developer">developer</ulink>
list: 
<ulink 
url="http://lists.arabeyes.org/archives/developer/2003/February/msg00150.html">
http://lists.arabeyes.org/archives/developer/2003/February/msg00150.html</ulink>
      </para></listitem>
    </itemizedlist>
  <!-- End of CREDITS Section -->
  </sect2>

  <sect2 id="translations"><title>Translations</title>
    <para>
Currently there are no known translations of this document.
    </para>
  <!-- End of TRANSLATIONS Section -->
  </sect2>
  <sect2 id="feedback"><title>Feedback</title>
    <para>
If you have any questions or suggestion, please do make them known on
the 'doc' mailing list here:
<ulink url="http://lists.arabeyes.org/mailman/listinfo/doc">
http://lists.arabeyes.org/mailman/listinfo/doc</ulink>
    </para>
  <!-- End of FEEDBACK Section -->
  </sect2>
<!-- End of INTRO Section -->
</sect1>

<sect1 id="images"><title>Graphic Images</title>
  <para>
Designing graphic images requires not only talent but skill at using the
available tools. You may either hand-draw something on paper and then scan it,
or use a vector drawing program. Then you can manipulate the image to its
final form, adding effects and final touches using an image manipulation
program.
  </para>
  <para>
If you are going to use a vector drawing program, there are several for
Linux.
  </para>
  <itemizedlist>
    <listitem><para>
<ulink url="http://www.koffice.org/kontour">Kontour</ulink> or now
<ulink url="http://www.koffice.org/karbon/index.phtml">Karbon14</ulink> - 
A vector drawing program for KDE which is bundled with Koffice as well.
    </para></listitem>
    <listitem><para>
<ulink url="http://www.linotux.ch/gestalter/">Gestalter</ulink> - 
a free vector drawing program. The user interface is loosely modeled after 
Adobe Illustrator. The central element is the Bezier curve used as a base part
for almost every other object. Complex paths are possible, compound paths can 
be constructed, grouping of elements is enabled, and everything can be 
screened by a mask. Adding and deleting of nodes is implemented as well as 
transformations (translation, rotation, scaling, and skewing).
    </para></listitem>
    <listitem><para>
<ulink url="http://sodipodi.sourceforge.net/">Sodipodi</ulink> - 
a general purpose vector drawing application which uses a subset of W3C SVG as
its file format. It uses an advanced imaging engine, with antialised display, 
apha transparency, and vector fonts.
    </para></listitem>
  </itemizedlist>
  <para>
Once you are satisfied with the image or if you don't use vector drawing
programs to begin with you can manipulate the image itself. This can
be done by programs such as <ulink url="http://www.gimp.org/">GIMP</ulink>.
  </para>
  <sect2 id="logo-banner"><title>Logos and Banners</title>
    <para>
We are constantly looking for new designs for logos and banners for Arabeyes, 
as well as the various individual projects. There is a distinction between
a logo and a banner that many people (incorrectly) fail to make.
    </para>
    <para>
A logo is a small graphic that is often simple (due to its small size) and
generally excludes text. That is unless it is a calligraphic type of logo
where the entire logo is essentially a drawn word/phrase. On the other
hand a banner is bigger and generally has text in it. 
    </para>
    <para>
To give a better idea, let us look at the specifications of each (and those
to be used for Arabeyes logos and banners).
    </para>
    <sect3 id="logo-specs"><title>Logo Specifications</title>
      <itemizedlist>
        <listitem><para>
Fill type (background) must be transparent<footnote><para>This is not
available in the JPEG format.</para></footnote>.
        </para></listitem>
        <listitem><para>
Image file format must be open and free from license restrictions.
We recommend <ulink url="http://www.libpng.org/pub/png/">PNG</ulink>.
        </para></listitem>
        <listitem><para>
Dimensions should be 100px x 100px (or very close to it, give or take
10 pixels)
        </para></listitem>
      </itemizedlist>
    <!-- End LOGO-SPECS Section -->
    </sect3>
    <sect3 id="banner-specs"><title>Banner Specifications</title>
      <para>
There are two types of banners for Arabeyes. One that is displayed on
Arabeyes.org pages and another are the ones deployed on other sites to
advertise/recruit volunteers. Because the latter may have other specifications
imposed by the hosting site, you may have to check with them. However,
a good rule is to follow our standard and adjust as you are being asked
to by the host.
      </para>
      <itemizedlist>
        <listitem><para>
Fill type (background) must be transparent.
        </para></listitem>
        <listitem><para>
Image file format must be open and free from license restrictions.
We recommend <ulink url="http://www.libpng.org/pub/png/">PNG</ulink>.
        </para></listitem>
        <listitem><para>
Dimensions should be 160px (length), 100px (height), or very close to it, 
give or take 10 pixels.
        </para></listitem>
      </itemizedlist>
    <!-- End BANNER-SPECS Section -->
    </sect3>
  <!-- End LOGO-BANNER Section -->
  </sect2>
  <sect2 id="icons"><title>Icons</title>
    <para>
Icons are those little nifty looking images that identify an application on
your desktop or in your menu. They are also found inside GUI applications
(e.g. Open File icon, Save icon, etc). The following are some of the most
popular free icon editors.
    </para>
    <itemizedlist>
      <listitem><para>
<ulink url="http://freshmeat.net/redir/kiconedit/5145/url_homepage/prog.html#KICONEDIT">KIconEdit</ulink> - 
The KDE Icon Editor is a small graphics drawing program especially for 
creating icons using the standard KDE icon palette. It has most of the tools 
which are needed for easy creation of icons including Line, Ellipse, Circle, 
Rectangle, Freehand, Eraser, and Spraycan.
      </para></listitem>
      <listitem><para>
<ulink url="http://savannah.nongnu.org/projects/giconed/">GNOME Iconedit</ulink> - 
The GNOME icon editor<footnote><para>As of the writing of this document it
seems that the project has not fully moved to Savannah yet.</para></footnote>.
      </para></listitem>
    </itemizedlist>
  <!-- End of ICONS Section -->
  </sect2>
<!-- End of IMAGES Section -->
</sect1>

<sect1 id="fonts"><title>Fonts</title>
  <para>
Designing fonts is whole art on its own and requires a tremendous amount
of patience and consistency. The first thing you need to have are font
charts. Once you have the charts you will need the necessary tools to draw
the fonts.
  </para>
  <sect2 id="font-charts"><title>Unicode Charts</title>
    <para>
Here is a list of the <ulink url="http://www.unicode.org/">Unicode</ulink> charts
that relate to Arabic:
    </para>
    <itemizedlist>
      <listitem><para>
<ulink url="http://www.unicode.org/charts/PDF/U40-0600.pdf">
http://www.unicode.org/charts/PDF/U40-0600.pdf</ulink>
      </para></listitem>
      <listitem><para>
<ulink url="http://www.unicode.org/charts/PDF/U40-FB50.pdf">
http://www.unicode.org/charts/PDF/U40-FB50.pdf</ulink>
      </para></listitem>
      <listitem><para>
<ulink url="http://www.unicode.org/charts/PDF/UFE70.pdf">
http://www.unicode.org/charts/PDF/UFE70.pdf</ulink>
      </para></listitem>
    </itemizedlist>
    <para>
Pay special attention to the following:
    </para>
    <itemizedlist>
      <listitem><para>
Punctuations: 060C, 061B, 061F, 0640, 064B, FD3E, FD3F
      </para></listitem>
      <listitem><para>
Harakat: 064B - 066D, 0670
      </para></listitem>
      <listitem><para>
Alef Wasla: FB50, FB51
      </para></listitem>
      <listitem><para>
Word Ligatures: FDF2
      </para></listitem>
    </itemizedlist>
  <!-- End of FONT-CHARTS Section -->
  </sect2>
  <sect2 id="font-tools"><title>Font Tools</title>
    <para>
There are several free font editing/modifying/converting tools out there,
in addition to commercial ones of course. For a quick discussion on font
types, you might want to check the 
<ulink url="http://www.tldp.org/HOWTO/Font-HOWTO/">Font HOWTO</ulink>. Most 
sepecifically, Section 2.1 titled "Types of Fonts".
    </para>
    <para>
Here is a list of some free font editors:
    </para>
    <itemizedlist>
      <listitem><para>
<ulink url="http://pfaedit.sourceforge.net">pfaedit</ulink> -
Postscript Font Editor (supports: postscript, TrueType, OpenType, cid-keyed 
and BDF).
      </para></listitem>
      <listitem><para>
<ulink url="http://crl.NMSU.Edu/~mleisher/xmbdfed.html">xmbdfed</ulink> - 
BDF font editor (supports: PK/GF, HBF, PSF, CP, FNT, vfont (Sun-console), 
Windows FON/FNT, TrueType, etc).
      </para></listitem>
      <listitem><para>
<ulink url="http://www.gnu.org/software/gfe">GFE</ulink> - 
GNU's Font Editor (supports: BDF and PCF fonts).
      </para></listitem>
    </itemizedlist>
    <para>
It is not enough to be able to create fonts. You may also want to convert
fonts from one format to another. Here are some useful converters:
    </para>
    <itemizedlist>
      <listitem><para>
<ulink url="http://crl.NMSU.Edu/~mleisher/ttf2bdf.html">ttf2bdf</ulink> - 
A TTF to BDF font format converter.
      </para></listitem>
      <listitem><para>
<ulink url="http://ttf2pt1.sourceforge.net">ttf2pt1</ulink> - 
A TTF to Postscript (type-1) font format converter.
      </para></listitem>
      <listitem><para>
<ulink url="http://www.tsg.ne.jp/GANA/S/pcf2bdf">pcf2bdf</ulink> - 
A PCF to BDF font format converter. 
      </para></listitem>
    </itemizedlist> 
  <!-- End of FONT-TOOLS Section -->
  </sect2>
<!-- End of FONTS Section -->
</sect1>

<sect1 id="publish"><title>Publishing Your Work</title>
  <para>
Now that you have found the tools you need and want to share your new
creations with the rest of the team you will need to find a place to
publish your temporary work. There are many free webhosting services out
there on the Internet, among them:
  </para>
  <itemizedlist>
    <listitem><para>
<ulink url="http://www.geocities.com/">GeoCities</ulink>
    </para></listitem>
    <listitem><para>
<ulink url="http://angelfire.lycos.com/">Angelfire</ulink>
    </para></listitem>
    <listitem><para>
<ulink url="http://www.tripod.lycos.com/">Tripod</ulink>
    </para></listitem>
  </itemizedlist>
  <para>
You may also want to check with the following links for a more comprehensive 
listing of available free webhosts.
  </para>
  <itemizedlist>
    <listitem><para>
<ulink url="http://www.webhosts4free.com">WebHost4Free</ulink> - They have a
very nice list of free hosts.
    </para></listitem>
    <listitem><para>
<ulink url="http://www.freewebspace.net/">FreeWebSpace</ulink>
    </para></listitem>
  </itemizedlist>
  <para>
Once you have found your webhost, you will want to let everyone know about your
new page. This is done via the appropriate Arabeyes
<ulink url="http://lists.arabeyes.org/mailman/listinfo">mailing-list</ulink>.
  </para>
<!-- End of PUBLISH Section -->
</sect1>

</article>
